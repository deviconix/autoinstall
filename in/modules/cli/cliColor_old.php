<?php

// Функция для установки цвета
function setColor($textColor, $backgroundColor)
{
    echo "\e[" . $backgroundColor . ";" . $textColor . "m";
}


function resetColor()
{
    //echo "\033[0m";
    echo "\e[0m";
}

// Цветовые коды
$textRed = "31"; // Красный текст
$textGreen = "32"; // Зеленый текст
$backgroundBlack = "40"; // Черный фон

// Функция для вывода ошибки
function strErr($mess)
{
    global $textRed, $backgroundBlack;
    setColor($textRed, $backgroundBlack);
    echo $mess;
    resetColor();
}


function strOk($mess)
{

    global $textGreen, $backgroundBlack;
    setColor($textGreen, $backgroundBlack);
    echo $mess;
    resetColor();
}

// usage example
//strErr("Ошибка: что-то пошло не так");
//strOk("Успех: операция выполнена успешно");
