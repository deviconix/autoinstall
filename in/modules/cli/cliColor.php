<?php

function setColor($textColor, $backgroundColor)
{
    return "\e[" . $backgroundColor . ";" . $textColor . "m";
}


function resetColor()
{
    //echo "\033[0m";
    return "\e[0m";
}

// Color code
$textRed = "31"; // red
$textGreen = "32"; // green
$textYellow = "33"; // yellow
$backgroundBlack = "40"; // background black

// return str
function strErr($mess)
{
    global $textRed, $backgroundBlack;
    $messColor = setColor($textRed, $backgroundBlack) . $mess . resetColor();
    return $messColor;
}

// return str
function strOk($mess)
{

    global $textGreen, $backgroundBlack;
    $messColor = setColor($textGreen, $backgroundBlack) . $mess . resetColor();
    return $messColor;
}

function strTitle($mess)
{
    global $textYellow, $backgroundBlack;
    $messColor = setColor($textYellow, $backgroundBlack) . $mess . resetColor();
    return $messColor;
}
