<?php
// Функция для установки цвета
function setColor($textColor, $backgroundColor)
{
    echo "\033[" . $backgroundColor . ";" . $textColor . "m";
}

// Функция для сброса цвета
function resetColor()
{
    echo "\033[0m";
}

// Цветовые коды
$textRed = "31"; // Красный текст
$textGreen = "32"; // Зеленый текст
$backgroundBlack = "40"; // Черный фон

// Функция для вывода ошибки
function strErr($mess)
{
    global $textRed, $backgroundBlack;
    setColor($textRed, $backgroundBlack);
    //echo $mess . "\n";
    echo $mess;
    resetColor();
}

// Функция для вывода успешного сообщения
function strOk($mess)
{
    global $textGreen, $backgroundBlack;
    setColor($textGreen, $backgroundBlack);
    //echo $mess . "\n";
    echo $mess;
    resetColor();
}

// Пример использования
//strErr("Ошибка: что-то пошло не так");
//strOk("Успех: операция выполнена успешно");
