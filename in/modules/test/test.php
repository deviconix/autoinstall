<?php

// Using the test function with an anonymous function and expect
function test($description, $callback)
{
    echo $description . "\n";
    $callback();
}
