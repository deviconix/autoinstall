<?php
// from test.php request configArr ?

// include all files in module test
// $test = $config["moduleTest"] . '/test.php';
// $expect = $config["moduleTest"] . '/expect.php';
// $toBe = $config["moduleTestConditions"] . '/toBe.php';

// require_once $test;
// require_once $expect;
// require_once $toBe;
logdebug('include : initTest.php');

appImportFunction('test', '/test.php');
appImportFunction('test', '/expect.php');
appImportFunction('TestConditions', '/toBe.php');

logdebug('include exit() : initTest.php');
