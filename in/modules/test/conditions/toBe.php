<?php

// compares 2 parameters
// Define the toBe method that will be used to check the value
function toBe($expected)
{
    return function ($actual) use ($expected) {
        if ($actual === $expected) {
            //$messOk = "OK - Test passed: {$actual} is equal to {$expected}\n";
            $messOk = "OK - Test passed:\n";
            echo strOk($messOk);
        } else {
            $messErr = "ERR - Test failed: {$actual} is not equal to {$expected}\n";
            echo strErr($messErr);
        }
    };
}
