<?php

// Define an expect function that takes a value and a test function
function expect($value, $assertion)
{
    // $test = toBe(3); // toBe(3) возвращает функцию, которая будет сравнивать с 3
    // $test(3); // Вывод: OK - Test passed: 3 is equal to 3
    // $test(4); // Вывод: ERR - Test failed: 4 is not equal to 3
    $assertion($value);
}
