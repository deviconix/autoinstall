<?php
//echo "\logdebug\n";
// relation app.php (root scenario , test.php, testDemo.php)
// beaute log 
// global $tabCode = 0;
function logdebug($mess)
{
    //echo "\nrun - logdebug\n";
    global $debug, $tabCode;

    $parts = explode(":", $mess);

    // Получение и обрезка первой части, если она существует
    $command = isset($parts[0]) ? trim($parts[0]) : '';
    // tabulation
    if ($command == 'run') {
        $tabCode += 1;
    }

    $tabulation = repeatString('    ', $tabCode);
    if ($debug) {
        echo "\n$tabulation$mess\n";
    }

    if ($command == 'exit') {
        $tabCode -= 1;
    }
}
