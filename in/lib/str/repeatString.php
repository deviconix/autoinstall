<?php
//echo "\nrepeatString\n";
function repeatString($string, $count)
{
    //echo "\nrun - repeatString\n";
    if ($count < 0) {
        return '';
    }

    $repeatedString = str_repeat($string, $count);

    return $repeatedString;
}
