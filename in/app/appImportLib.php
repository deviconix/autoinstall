<?php

// Relation : /modules/path/getPathModule.php
// import function
//require_once $root . '/modules/path/getPathModule.php';
logdebug('include : appImportLib.php');
// public
function appImportLib($module, $functionName)
{

    logdebug("run : appImportLib( $module$functionName )");
    // private
    //importFunction($config,$module, $functionName.'.php');
    //$pathFunction = getPathLib($module) . $functionName; // include app/test
    $prefix = 'lib';
    $pathFunction = getPath($prefix, $module) . $functionName; // include app/test
    //var_dump($pathFunction);
    logdebug("exit : appImportLib( $module$functionName )");
    require_once $pathFunction;
}
