<?php

// Relation : /modules/path/getPathModule.php
// import function
//require_once $root . '/modules/path/getPathModule.php';
logdebug('include : appImportFunction.php');
// public
function appImportFunction($module, $functionName)
{
    logdebug("run : appImportFunction( $module$functionName )");
    // private
    //importFunction($config,$module, $functionName.'.php');
    //$pathFunction = getPathModule($module) . $functionName; // include app/test
    $prefix = 'module';
    $pathFunction = getPath($prefix, $module) . $functionName; // include app/test
    //var_dump($pathFunction);
    logdebug("exit : appImportFunction( $module$functionName )");
    require_once $pathFunction;
}
