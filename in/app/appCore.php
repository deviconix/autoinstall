<?php
// view err (debug)
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//+ INIT LIBS
//not work appImportLib('str', '/repeatString.php');
//not work appImportFunction('log', '/logdebug.php'); // relative : repeatString()
require_once $root . '/modules/cli/cliColor.php'; // for module/test color strErr(red),strOk(green)
require_once $root . "/lib/str/repeatString.php"; // 
require_once $root . "/modules/log/logdebug.php"; // import $config<array>

// IN <- global $root
logdebug('start : appCore.php');
logdebug('run : include');

logdebug("test : color red " . strErr('ERROR'));
logdebug('test : color green ' . strOk('OK'));
// GLOBAL

// include config
require_once $root . "/config/config.php"; // import $config<array>


require_once $root . '/modules/path/getPathSubRoot.php';

require_once $root . '/modules/path/getPath.php'; // relative:getPathSubRoot()

//require_once $root . '/modules/cli/cliColor.php'; // for module/test color strErr(red),strOk(green)

// Relation : /modules/path/getPathModule.php
// FUNCTION requery_once replace with appImportFunction('name(module/name)','/functionName.php')
require_once $root . '/app/appImportFunction.php'; // import appImportFunction()
// Relation : /modules/path/getPathModule.php
require_once $root . '/app/appImportTest.php'; // import appImportTest()

//appImportFunction('config', 'config.php');(key=path, '/file.php')
logdebug('exit : include');
logdebug('end  : appCore.php');
