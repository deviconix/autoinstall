<?php

// Relation : /modules/path/getPathModule.php
// import function
//require_once $root . '/modules/path/getPathModule.php';
logdebug('include : appImportTest.php');
// public
function appImportTest($module, $functionName)
{
    logdebug("run : appImportTest( $module$functionName )");
    // private
    //importFunction($config,$module, $functionName.'.php');
    //$pathFunction = getPathTest($module) . $functionName; // include app/test
    $prefix = 'test';
    $pathFunction = getPath($prefix, $module) . $functionName; // include app/test
    //var_dump($pathFunction);
    logdebug("exit : appImportTest( $module$functionName )");
    require_once $pathFunction;
}
