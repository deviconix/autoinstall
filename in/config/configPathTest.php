<?php
logdebug('include : configPathTest.php');


$configPathTest = [
    "testPath" => $root . "/test/path",
    "testDemo" => $root . "/test/demo",
    "libStr" => $root . "/lib/str",
    "modulePath" => $root . "/modules/path",
    "moduleTest" => $root . "/modules/test",
    "moduleTestConditions" => $root . "/modules/test/conditions",
];
