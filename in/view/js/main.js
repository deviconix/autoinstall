let logInterval;

function fetchLog() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'fetch_log.php', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('log').innerHTML = xhr.responseText.replace(/\n/g, '<br>');
            document.getElementById('log').scrollTop = document.getElementById('log').scrollHeight;
            if (xhr.responseText.includes("Установка завершена")) {
                clearInterval(logInterval);
                document.getElementById('start-button').disabled = false;
            }
        }
    };
    xhr.send();
}

function startInstallation() {
    document.getElementById('start-button').disabled = true;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'snipeit_install.php', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log('Installation script started');
        }
    };
    xhr.send();
    logInterval = setInterval(fetchLog, 3000);
    fetchLog();
}