<?php
// demo
function sum($a, $b)
{
    return $a + $b;
}

function getPathRootTest($config)
{

    test('TEST 1 : getPathRootTest()  DEMO TEST (adds 1 + 2 to equal 3)', function () {
        expect(sum(1, 2), toBe(3));
    });
};

function getPathRootTestErr($config)
{

    test('TEST 1-1 : getPathRootTest()  DEMO TEST - ERROR (adds 1 + 2 to equal 4)', function () {
        expect(sum(1, 2), toBe(4));
    });
};
