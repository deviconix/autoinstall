<?php
//echo "\n";
//echo "pathAll TEST\n";

function pathTest($config)
{
    //echo "\n";
    $title1 = "\n-------------------- RUN TESTS ---------------------\n";
    $title2 = "\nPackage path:\n";
    echo strTitle($title1);
    echo strTitle($title2);


    // 
    $pathTest = $config["testPath"] . "/getPathRoot.test.php";

    include_once $pathTest;
    // TEST 1
    //+demo    
    getPathRootTest($config);
    getPathRootTestErr($config);

    // TEST 2
    // getPathModule - public
    $pathTest = $config["testPath"] . "/getPathModule.test.php";
    include_once $pathTest;
    getPathModuleTest($config);
}
