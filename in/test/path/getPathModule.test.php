<?php
// include functionTest
//+$pathFunctionForTest = $config["modulePath"] . '/getPath.php';
//+require_once $pathFunctionForTest;

appImportFunction('path', '/getPath.php');

function getPathModuleTest($config)
{

    // echo "\nRUN - getPathModuleTest\n";

    test("\nTEST 2 : getPathModuleTest() (equal __DIR__../modules/path with \$config['modulePath'])", function () {
        //expect(sum(1, 2), toBe(3));
        global $root;

        $expectedValue = $root . '/modules/path'; // constTest
        //$functionResult = getPathModule('path'); // function()
        $functionResult = getPath('module', 'path'); // function()

        expect($functionResult, toBe($expectedValue));
    });
};
