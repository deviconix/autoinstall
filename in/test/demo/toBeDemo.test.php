<?php
// demo
function sum($a, $b)
{
    return $a + $b;
}


function toBeDemoTest($config)
{
    echo "RUN ++++++ getPathRoot TEST\n";

    test('adds 1 + 2 to equal 3', function () {
        expect(sum(1, 2), toBe(3));
    });
};
