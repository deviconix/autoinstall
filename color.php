<?php
// Проверка, обрабатываются ли escape-последовательности
echo "Normal text\n";
echo "\033[31mThis should be red text\033[0m\n";
echo "\033[32mThis should be green text\033[0m\n";
echo "Back to normal text\n";

// Пример с фоном
echo "\033[40;31mThis is a red text on black background\033[0m\n";
echo "\033[40;32mThis is a green text on black background\033[0m\n";
