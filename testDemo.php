<?php
// entry point

// state debug
$debug = true;
//$debug = false;

// beaute log 
$tabCode = 0;

// SET PATH rootProject
$rootProject = __DIR__;

// SET PATH root FOR APP 'in'
$root = __DIR__ . '/in';

// analog autoload.php
require_once $root . "/app/appCore.php"; // import $config<array>

// INIT MODULE TEST +++ FROM CONFIG
//! rename appImportModuleFunction (...);
appImportFunction('test', '/initTest.php');

//---------------------------- PUBLIC MODULE

// INCLUDE TEST
//appImportTest('demo' - prefixFolderName in $config (prefix not write), ...);
appImportTest('demo', '/toBeDemo.test.php');

// RUN TEST
toBeDemoTest($config);
